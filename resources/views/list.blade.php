@extends('layouts.master')
@section('title','List')
@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css')}}">
@endsection
@section('content')
    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('message') }}
        </div>
    @endif
    {{-- <h1 class="main-red">List Pager</h1> --}}
    <table class="table table-dark text-center">
        <thead>
            <th>ID</th>
            <th>FNAME</th>
            <th>LNAME</th>
            <th>Date_Create</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($people as $item)
              <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->fname }}</td>
                <td>{{ $item->lname }}</td>
                <td>{{ $item->created_at }}</td>
              <td class="form-inline justify-content-center">
                  <a class="btn btn-success" href="{{ url('people/'.$item->id.'/edit') }}">Edit</a>
              <form class="ml-2" action="{{ url('people',[$item->id]) }}" method="POST">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger">Delete</button>
              </form>
            </td>
              </tr>  
            @endforeach
            
        </tbody>
    </table>
@endsection
