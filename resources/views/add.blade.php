@extends('layouts.master')
@section('title','Add')
@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css')}}">
@endsection
@section('content')
<form action="{{ url('people') }}" method="post">
  @csrf
    <div class="form-group">
      <label for="my-input">Firstname</label>
      <input id="my-input" class="form-control" type="text" name="fname">
    </div>
    <div class="form-group">
        <label for="my-input">Lastname</label>
        <input id="my-input" class="form-control" type="text" name="lname">
    </div>
    <div class="form-group">
        <label for="my-input">Age</label>
        <input id="my-input" class="form-control" type="text" name="age">
    </div>
    <button type="submit" class="btn btn-success">Save</button>

    @if($errors->any())
    <div class="alert alert-danger mt-3" >
      <ul>
        @foreach ($errors->all() as $err)
          <li>{{ $err }}</li>
        @endforeach
      </ul>
  </div>
  @endif
</form>
@endsection