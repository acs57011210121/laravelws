<nav class="navbar navbar-expand-lg navbar-light bg-light ">
  <a class="navbar-brand" href="/">PANDA</a>
  <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div id="my-nav" class="collapse navbar-collapse">
    <ul class="navbar-nav mr-auto">
      <li class="{{ Request::segment(1) === 'people' && Request::segment(2) === 'create' ? 'active' : null }}">
        <a class="nav-link" href="/people/create" tabindex="-1" aria-disabled="true">Add</a>
      </li>
      <li class="{{ Request::segment(1) === 'people' && Request::segment(2) == '' ? 'active' : null }}">
          <a class="nav-link" href="/people">List</a>
        </li>
    </ul>
  </div>
</nav>